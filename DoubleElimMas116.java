package cs21120.assignment2017.solution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;
import java.util.LinkedList;

public class DoubleElimMas116 implements IManager {
	
	//queue with winners and losers players(team)
	private Queue<String> winners = new LinkedList<String>();
	private Queue<String> losers = new LinkedList<String>();
	
	//it helps to recognise in which queue was placed match 
	private Queue<String> present = winners;
	
	/* 
	 * 3 stacks for undo and redo methods
	 * First stack is collecting players 1 from match (p1)
	 * Second stack is collecting players 2 from match (p2)
	 * Third stack is collecting winners of the match
	 * 
	 * more about how these methods works in undo() and redo() 
	 */
	private Stack<String> uPosition1 = new Stack<>();
	private Stack<String> uPosition2 = new Stack<>();
	private Stack<String> uWinPlayers = new Stack<>();
	
	private Stack<String> rPosition1 = new Stack<>();
	private Stack<String> rPosition2 = new Stack<>();
	private Stack<String> rWinPlayers = new Stack<>();
	
	/*
	 * p1 - player(team) 1
	 * p2 - player(team) 2
	 * winner - winner of last match
	 */
	private String p1;
	private String p2;
	private String winner="";
	
	//2 variables keeps two steps of statement about hasNextMatch()
	private boolean hasN = false;
	private boolean hasNext= false;
	
	//if it changes for true if match is final
	private boolean finalTime = false;
	
	@Override
	public void setPlayers(ArrayList<String> players) {
	
		/*
		 * All players(teams) start competition from winners queue
		 * They are added to winners with the same order as in file
		 */
		
		for(String p : players) {
			winners.add(p);
		}	
	}
	public boolean hasNextMatch() {
		
		/*
		 * Method returns false if there is no more match in competition
		 * and true if there is.
		 * Variable hasNext is changing in setMatchWinner, because
		 * this is the best time to check if next match is
		 */
		if(hasNext) {
			finalTime = true;
			return false;
		}
		else 
			return true;
	}
	public Match nextMatch() throws NoNextMatchException {

/*
 * method recognises 3 cases:
 * 		- final (when size of w and l arrays is equal 1)
 * 			In this case method takes players from two queues
 * 			Also it notices that it is the final(last match)
 * 		- match in winners queue
 * 			It takes players from winner queue.
 * 			It saves current queue in variable present to remember where match was played
 * 		- match in losers queue
 * 			The same as in winners queue.
 *			There is only one difference, it takes and saves from losers queue
 * 
 * Then it creates object Match with two players
 */

		if(winners.size() == 1 && losers.size() == 1) {
			//final, last match
			p1 = winners.poll();
			p2 = losers.poll();
			hasN=true;
			
		}
		else if(winners.size() >= losers.size()) {
			//winners queue
			p1 = winners.poll();
			p2 = winners.poll();
			present = winners;	
		} else {
			//losers queue
			p1 = losers.poll();
			p2 = losers.poll();
			present = losers;
		}
		//Create object Match type and return it.
		Match match = new Match(p1, p2);
		return match;	
	}
	@Override
	public void setMatchWinner(boolean player1) {
		
		/*
		 * Task of this method is set a winner of a match
		 * If it is not a final add to undo stacks
		 * 
		 * During creating match in variable present was saved data from winners or losers queue.
		 * In first case present is the same as winners. It means that match was played in winners queue
		 * If not it means that they played in losers queue.
		 * 
		 *  At the end of method we check if it's last match.
		 */
		if(!finalTime) {
			uPosition1.push(p1);
			uPosition2.push(p2);	
		}
	
		//winners
		if(present.equals(winners)) {
			//p1 wins
			if(player1) {
				uWinPlayers.push(p1);
				winners.add(p1);
				losers.add(p2);
				winner=p1;

				
			}
			//p2 won
			else {
				uWinPlayers.push(p2);
				winners.add(p2);
				losers.add(p1);
				winner=p2;

			}
			//losers
		}else {
			if(player1) {
				uWinPlayers.push(p1);
				losers.add(p1);
				winner=p1;

				
			}
			else {
				losers.add(p2);
			}
		}
		if(hasN) 
			hasNext=true;
	}

	@Override
	public String getWinner() {
		/*
		 * Winner of competition is a player(team) who won in final match.
		 * 
		 * To check if it's final method is using variable fainalTime, which changes value to true if match is final.
		 * If it is not, it returns null.
		 */
		if(finalTime)
			return winner;
		else
			return null;
		
	}

	@Override
	public void undo() {		
		
		/*
		 * create 3 variables and pop tops of these stacks
		 * win - winner of match
		 * pla1 - player from position 1
		 * pla2 - player from position 2
		 */
		String win = uWinPlayers.pop();
		String pla1 = uPosition1.pop(); 
		String pla2 = uPosition2.pop();
		
		/*
		 * to save actions from undo method saves whole changing data
		 * in special stacks for redo method
		 */
		rPosition1.add(pla1);
		rPosition2.add(pla2);
		rWinPlayers.add(win);
		
	
	if( !finalTime) {
		//This is normal match (not final)

		if(winners.contains(win)) {
			//match was played in winners queue
			
			//Queue type doesn't support indexing as new Queue queueWinners helps transport data
			Queue<String> queueWinners = new LinkedList<String>();
			queueWinners.addAll(winners);
			
			/*
			 * To take it back in the same order
			 * First what we do is clearing whole queue
			 * It was copy to queueWinners so we dont lose any data
			 * In first and second place method adds players 1 and 2 from last played match
			 * next players from next match
			 * and rest of data 
			 * I'm using loop and if to dont duplicate data
			 */
			winners.clear();
			winners.add(pla1);
			winners.add(pla2);
			winners.add(p1);
			winners.add(p2);

			for(String q : queueWinners) {
				if( !winners.contains(q) ) {
					winners.add(q);
				}
			}
			/*
			 * Next step is remove players who lost last match.
			 * At the moment he is inside losers queue.
			 * It must vanish from there to avoid redundancy data.
			 * For this it's created new queue to keep data from Queue losers
			 * Also here I'm using loops and if's to avoid redundancy and lose some data
			 */
			Queue<String> queueLosers = new LinkedList<String>();
			queueLosers.addAll(losers);
			losers.clear();
			for(String l : queueLosers) {
				if( win == pla1 ) {
					//player 1 won
					if( l != pla2){
						//if player from queueLosers is not player2 add it to losers
						losers.add(l);
					}
				}
				else {
					//player 2 won
					if( l != pla1) {
						//if player from queueLosers is not player1 add it to losers
						losers.add(l);
					}
				}
			}
			
		}
		else {
			Queue<String> queue = new LinkedList<String>();
			queue.addAll(losers);
			losers.clear();
			losers.add(pla1);
			losers.add(pla2);
			losers.addAll(queue);
		}
	}
	else {
		//final
		winners.clear();
		losers.clear();
		winners.add(p1);
		losers.add(p2);
		finalTime = false;
		hasNext = false;

	}
	}
	@Override
	public void redo() {
		String pos1;
		String pos2;
		String win;
		
		pos1 = rPosition1.pop();
		pos2 = rPosition2.pop();
		win = rWinPlayers.pop();
		
		uPosition1.add(pos1);
		uPosition2.add(pos1);
		uWinPlayers.add(win);
		
		if(winners.contains(win)) {
			//match was played in winners queue
			if(win.equals(pos1)) {
				//pos1 won with pos2
				winners.add(pos1);
				losers.add(pos2);
			}
			else if(win.equals(pos2)) {
				//pos2 won with pos1
				winners.add(pos2);
				losers.add(pos1);
			}
		}
		else {
			//the match was played in losers queue
			if(win.equals(pos1)) {
				//pos1 won with pos2
				losers.add(pos1);
			}
			else{
				//pos2 won with pos1
				losers.add(pos2);
			}
		}
	}

	@Override
	public boolean canUndo() {
		/*
		 * if any match was played or every match was done undo
		 * return false
		 */
		
		if(uWinPlayers.isEmpty())
			return false;
		else
			return true;
	}

	@Override
	public boolean canRedo() {
		/*
		 * to use Redo first we have to use undo
		 * In undo we save to rWinLpayers data to redo
		 * if it's empty it returns false
		 */
		
		if(rWinPlayers.isEmpty())
			return false;
		else
			return true;
	}
	
 	public static void main (String[] args) throws FileNotFoundException {
		DoubleElimMas116 manager = new DoubleElimMas116();
		CompetitionManager cm = new CompetitionManager(manager);
		args = new String[2];
		args[0] = manager.getClass().getTypeName();
		args[1] = "list.txt";
		CompetitionManager.main(args);
	}

 	/*
 	 * Self-evaluation
 	 * I tried to the best how I was able to do.
 	 * I didn't know what form of text I should include (use: first form - I or WE or don't use any forms)
 	 */
 	
}